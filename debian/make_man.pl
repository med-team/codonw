#!/usr/bin/perl -w

die "Usage: $0 TEMPLATE OUTDIR\n" if @ARGV < 2;

my %tools = (
	rscu => ['calculate Relative Synonymous Codon Usage (RSCU)','rscu'],
	cu => ['calculate codon usage','cu'],
	aau => ['calculate Amino Acid Usage (AAU)','aau'],
	fop => ['calculate Frequency of OPtimal codons index (FOP)','fop'],
	raau => ['calculate Relative Amino Acid Usage (RAAU)','raau'],
	tidy => ['clean up sequence in FASTA format','tidy'],
	reader => ['output sequence in reader format','reader'],
	cutab => ['tabulation of codon usage','cutab'],
	cutot => ['tabulation of datasets codon usage','cutot'],
	transl => ['conceptual translation of DNA to amino acid','transl'],
	bases => ['detailed report of base composition','base','base'],
	base3s => ['silent base G+C+A+T calculating program','sil_base'],
	dinuc => ['dinucleotide usage of the three codon positions','dinuc'],
	cai => ['calculate Codon Adaptation Index (CAI)','cai'],
	gc3s => ['GC of synonymous codons 3rd positions','gc3s'],
	gc => ['G+C content of gene (all 3 codon positions)','gc'],
	cbi => ['calculate Codon Bias Index (CBI)','cbi'],
	enc => ['calculate Effective Number of Codons (ENc)','enc']
);

open FILE, $ARGV[0] or die "Could not open file: $!";
$string = join("", <FILE>);
close FILE;

foreach my $tool (keys %tools) {
	my $tmpl = $string;
	my $uctool = uc $tool;
    print @{$tools{$tool}}[0] . "\n";
    open(my $fh, '>', "$ARGV[1]/codonw-$tool.1") or die "Could not open output file $!";
    $tmpl =~ s/%%TOOLNAME%%/codonw-$tool/g;
    $tmpl =~ s/%%CTOOLNAME%%/CODONW-$uctool/g;
    $tmpl =~ s/%%TOOLDESC%%/@{$tools{$tool}}[0]/g;
    $tmpl =~ s/%%TOOLOPT%%/@{$tools{$tool}}[1]/g;
    print $fh "$tmpl";
    close $fh;
}